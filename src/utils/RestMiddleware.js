import superagent from 'superagent';
import {notify} from 'react-notify-toast';

import { host } from './Consts';

export const callRestEndpoint = (startDate, endDate, callback) => {
  superagent.get(host+"/report/"+startDate.format('MM-DD-YYYY')+"/"+
    endDate.format('MM-DD-YYYY'))
  .type('application/json')
  .end((err, res) => {
    if(err) {
      notify.show("Something went wrong :( " + err, "error");
      return;
    }
    notify.show("success", "success");
    let data = JSON.parse(res.text);
    callback(data);
  })
}
