import _ from 'lodash';
import moment from 'moment';

import { DAY, WEEK, MONTH } from './Consts';

export const recalculateEnergyCost = (cost, data) => {
  data.dayConsumption.map(consumption =>
      consumption.price = calculatePrice(consumption.consumption, cost));
  return data;
}

export const groupDataBy = (gb, data) => {
  switch (gb) {
    case DAY:
      return data.get(DAY)
    case WEEK:
      return data.get(WEEK) ? data.get(WEEK) : groupBy(data.get(DAY), WEEK);
    case MONTH:
      return data.get(MONTH) ? data.get(MONTH) : groupBy(data.get(DAY), MONTH);
    default:
      return data;
  }
}

const groupBy = (data, groupBy) => {
  let userData = {...data};
  let mapOfDateToListOfDates = _.groupBy(userData.dayConsumption,
        (result) => moment(result.date).startOf(groupBy).format("YYYY-MM-DD"));
  let arr = Object.keys(mapOfDateToListOfDates)
        .map(key => Object.assign({}, {date: key}, {consumption:_.sumBy(mapOfDateToListOfDates[key], 'consumption')}));
  userData.dayConsumption = arr;
  return userData;
}

const calculatePrice = (amount, price) => {
  return Math.round(amount*price*100)/100;
}

export const roundFloat = (float) => {
  return parseFloat(float).toFixed(2);
}
