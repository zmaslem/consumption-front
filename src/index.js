import React from 'react';
import ReactDOM from 'react-dom';
import './css/react-bootstrap-table.min.css';
import './css/react-dates-overrides.css';
import './css/styles.css';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
