import React from 'react';

import DateSelector from './DateSelector';
import EnergyCostInput from './EnergyCostInput';
import { DataVisualizer } from './DataVisualizer';
import { GroupBySelector } from './GroupBySelector';
import { recalculateEnergyCost, groupDataBy } from './../utils/Calculations';
import { DAY } from './../utils/Consts';

export default class ConsumptionContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: new Map(),
      energyCost: 0,
      groupBy: DAY
    }
  }

  onDataAppear = (newData) => {
    let userData = new Map();
    userData.set(DAY, newData);
    userData.set(this.state.groupBy, groupDataBy(this.state.groupBy, userData));
    userData.set(this.state.groupBy, recalculateEnergyCost(this.state.energyCost,
       userData.get(this.state.groupBy)));
    this.setState({userData:userData});
  }

  onCostInputApproved = (energyCost) => {
    this.setState({energyCost});
    if (!this.state.userData || this.state.userData.size === 0) {
      return
    }
    let userData = new Map(this.state.userData);
    userData.set(this.state.groupBy, recalculateEnergyCost(energyCost,
      userData.get(this.state.groupBy)));
    this.setState({userData});
  }

  onGroupBySelected = (groupBy) => {
    this.setState({groupBy: groupBy});
    if (!this.state.userData || this.state.userData.size === 0) {
      return
    }
    let userData = new Map(this.state.userData);
    userData.set(groupBy, groupDataBy(groupBy, userData));
    userData.set(groupBy, recalculateEnergyCost(this.state.energyCost,
      userData.get(groupBy)));
    this.setState({userData: userData});
  }

  render() {
    let userData = this.state.userData.get(this.state.groupBy);
    return (
      <div className="w-1200">
        <div className="header">USER ENERGY CONSUMPTION REPORT</div>
        <div className="input">
          <DateSelector onDataAppear={this.onDataAppear} />
          <GroupBySelector onGroupBySelected={this.onGroupBySelected}
            groupBy={this.state.groupBy} />
          <EnergyCostInput onCostInputApproved={this.onCostInputApproved}/>
        </div>
        <div className="result">
          <DataVisualizer userData={userData} energyCost={this.state.energyCost}/>
        </div>
      </div>
    )
  }
}
