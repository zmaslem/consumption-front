import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { roundFloat } from './../utils/Calculations';

export const DataVisualizer = (props) => {
    let dayConsumption = props.userData ? props.userData.dayConsumption : [];
    return <div>
      <div>
        <label>Total consumption: {props.userData ? props.userData.total : 0}</label>
        <label>Total price: {props.userData ? roundFloat(props.userData.total*props.energyCost) : 0}</label>
      </div>
      <BootstrapTable data={dayConsumption} striped={false} hover={true} pagination>
        <TableHeaderColumn dataField={DATE} isKey={true} dataSort={true}>Date</TableHeaderColumn>
        <TableHeaderColumn dataField={CONSUMPTION} dataFormat={formatRow} dataSort={true}>Consumption</TableHeaderColumn>
        <TableHeaderColumn dataField={PRICE} dataSort={true}>Energy Price [$]</TableHeaderColumn>
      </BootstrapTable>
    </div>
}

const formatRow = (cell, row) => {
  console.log(cell, row)
  return <div>{roundFloat(cell)}</div>
}

const DATE = "date";
const CONSUMPTION = "consumption";
const PRICE = "price";
