import React from 'react';
import moment from 'moment';

import { callRestEndpoint } from '../utils/RestMiddleware';
import DatePicker from './selector/DatePicker';

export default class DateSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment().subtract(2, 'years'),
      endDate: moment().subtract(1, 'days')
    }
  }

  onDatesChange = (startDate, endDate) => {
    this.setState({ startDate, endDate });
  }

  makeCall = () => {
    callRestEndpoint(this.state.startDate,
      this.state.endDate,
      this.props.onDataAppear)
  }

  render() {
    return <div className="inline">
      <label>Select range of dates</label>
      <DatePicker startDate={this.state.startDate} endDate={this.state.endDate}
        onDatesChange={this.onDatesChange}/>
      <button className="button" onClick={this.makeCall}>Submit</button>
    </div>
  }
}
