import React from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';

import { DAY, WEEK, MONTH } from './../utils/Consts';

export const GroupBySelector = (props) => {
  return <div className="inline">
    <label>Group By</label>
    <div>
      <DropdownButton
        title={props.groupBy}
        className="center"
        id="id"
      >
        {dropDownItems.map((item, index) => <MenuItem key={index} id={index} eventKey={item}
          onSelect={props.onGroupBySelected}>{item}</MenuItem>)}
        </DropdownButton>
    </div>
  </div>
}

const dropDownItems = [DAY, WEEK, MONTH];
