import React from 'react';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker } from 'react-dates';
import moment from 'moment';

export default class DatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      focusedInput: null
    }
  }

  isOutsideRange = (day) => {
    if(this.state.focusedInput === "endDate"){
      return day.isAfter(moment(new Date())) || day.isBefore(this.props.startDate);
    }
    if(this.state.focusedInput === "startDate"){
      return day.isAfter(moment(new Date())) || day.isAfter(this.props.endDate) || day.isBefore(moment(new Date()).add(-2, 'years'));
    }
    return false;
  }

  render() {
    return <div>
      <DateRangePicker
          startDateId="startDate"
          endDateId="endDate"
          startDate={this.props.startDate}
          endDate={this.props.endDate}
          onDatesChange={({ startDate, endDate }) => this.props.onDatesChange(startDate, endDate)}
          focusedInput={this.state.focusedInput}
          onFocusChange={(focusedInput) => { this.setState({ focusedInput })}}
          isOutsideRange={day => this.isOutsideRange(day)}
        />
    </div>
  }
}
