import React from 'react';

export default class EnergyCostInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: 0
    }
  }

  onChange = (value) => {
    this.setState({price: value.target.value});
  }

  onClick = () => {
    this.props.onCostInputApproved(this.state.price);
  }

  render() {
    return <div className="inline">
      <label>kW/h price</label>
      <input placeholder="0" type="number" onChange={this.onChange}/>
      <button className="button" onClick={this.onClick}>Confirm</button>
    </div>
  }
}
