import React, { Component } from 'react';
import Notifications from 'react-notify-toast';

import ConsumptionContainer from './components/ConsumptionContainer';

class App extends Component {
  render() {
    return (
      <div>
        <ConsumptionContainer />
        <Notifications />
      </div>
    );
  }
}

export default App;
