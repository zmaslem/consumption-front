This is the front end application that can be used for requesting and displaying
the user energy consumption based on start and end range of dates in two years in the past.

How to start:
1) clone the repository
2) npm install
3) npm start

Now you're ready to go! :)

How to use:
1) you should already have browser opened on localhost:3000. If no- navigate
  to before-mentioned url
2) click on date picker and choose your start and end dates
3) submit your choice with "Submit" button. The data should appear in the table
4) if you want to calculate the total price and single day/month/week price then
  provide the energy price and confirm it
4) optionally you can choose the grouping rule: by day, by week or by month.
  -by day: will display day by day data in rows with the consumption and price (if provided)
  -by week: will display week by week data. The displayed date is considered
    start date of the week of given data (startData)
  -by month: will display month by month data in rows. The displayed date is considered
    as the beginning of the given month (startData)

You will notice update of "Total consumption" and "Total price" values whenever you change
range of dates or kW/h price value.

Enjoy!
